package com.example.account;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.account.configuration.AccountConfiguration;
import com.example.account.dao.AccountDao;
import com.example.account.resources.AccountResource;

import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class AccountApplication extends Application<AccountConfiguration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountApplication.class);

    public static void main(String[] args) throws Exception {
        new AccountApplication().run(args);
    }

    @Override
    public String getName() {
        return "account-application";
    }

    @Override
    public void initialize(Bootstrap<AccountConfiguration> bootstrap) {
    }

    @Override
    public void run(AccountConfiguration configuration, Environment environment) {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment,
            configuration.getDataSourceFactory(),
            configuration.getDatabaseName());

        final AccountDao accountDao = jdbi.onDemand(AccountDao.class);

        try {
            accountDao.createTable();
        } catch (Exception ex) {
            LOGGER.info("WebToken table already exists");
        }
        
        final AccountResource accountResource = new AccountResource(accountDao);

        environment.jersey().register(accountResource);

    }
}
