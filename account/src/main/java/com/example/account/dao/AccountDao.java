package com.example.account.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.example.account.dao.mapper.AccountMapper;
import com.example.api.model.Account;

@RegisterMapper(AccountMapper.class)
public interface AccountDao {

    @SqlUpdate("create table ACCOUNT(ID INT PRIMARY KEY auto_increment, NUMBER VARCHAR(50), CURRENCY VARCHAR(50), USER VARCHAR(50))")
    void createTable();

    @SqlQuery("select * from ACCOUNT where USER = :user")
    List<Account> getAll(@Bind("user") String user);

    @SqlQuery("select * from ACCOUNT where ID = :id")
    Account findById(@Bind("id") int id);

    @SqlUpdate("delete from ACCOUNT where ID = :id")
    int deleteById(@Bind("id") int id);

    @SqlUpdate("update ACCOUNT set NUMBER = :number, CURRENCY = :currency where ID = :id")
    int update(@BindBean Account account);

    @SqlUpdate("insert into ACCOUNT (NUMBER, CURRENCY, USER) values (:number, :currency, :user)")
    int insert(@BindBean Account account);
}