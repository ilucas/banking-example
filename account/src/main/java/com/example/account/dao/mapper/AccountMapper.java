package com.example.account.dao.mapper;

import com.example.api.model.Account;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements ResultSetMapper<Account>
{
    public Account map(int index, ResultSet resultSet, StatementContext statementContext) throws SQLException
    {
        Account account = new Account(resultSet.getString("NUMBER"), resultSet.getString("CURRENCY"), resultSet.getString("USER"));
        account.setId(resultSet.getInt("ID"));

        return account;
    }
}
