package com.example.account.resources;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.account.dao.AccountDao;
import com.example.api.model.Account;

@Path("/accounts")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class AccountResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);

    AccountDao accountDao;

    public AccountResource(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @GET
    //@Secured(permission = "accountView")
    public List<Account> getAll(@HeaderParam("callerId") String callerId){
        LOGGER.info("Retrieving all accounts for callerId: " + callerId);

        return accountDao.getAll(callerId);
    }

    @GET
    @Path("/{id}")
    //@Secured(permission = "accountView")
    public Account get(@HeaderParam("callerId") String callerId, @PathParam("id") Integer id){
        LOGGER.info("Retrieving account" + id + " for callerId: " + callerId);

        return accountDao.findById(id);
    }

    @POST
    //@Secured(permission = "accountAdd")
    public Account add(@HeaderParam("callerId") String callerId, @Valid Account account) {
        LOGGER.info("Adding a account for callerId: " + callerId);

        int newId = accountDao.insert(account);
        account.setId(newId);
        return account;
    }

    @PUT
    @Path("/{id}")
    //@Secured(permission = "accountEdit")
    public Account edit(@HeaderParam("callerId") String callerId, @PathParam("id") Integer id, @Valid Account account) {
        LOGGER.info("Editing account " + id + " for callerId: " + callerId);

        account.setId(id);
        accountDao.update(account);

        return account;
    }

    @DELETE
    @Path("/{id}")
    //@Secured(permission = "accountDelete")
    public void delete(@HeaderParam("callerId") String callerId, @PathParam("id") Integer id) {
        LOGGER.info("Deleting account " + id + " for callerId: " + callerId);
        accountDao.deleteById(id);
    }
}