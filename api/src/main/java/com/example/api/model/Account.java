package com.example.api.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Account {

    @JsonProperty
    int id;

    @NotNull
    @JsonProperty
    String number;

    @NotNull
    @JsonProperty
    String currency;
    
    @NotNull
    @JsonProperty
	String user;

    public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String accountNumber) {
        this.number = accountNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String accountCurrency) {
        this.currency = accountCurrency;
    }
    
    public Account(String accountNumber, String accountCurrency) {
        this.number = accountNumber;
        this.currency = accountCurrency;
        this.user = "";
    }

    public Account(String accountNumber, String accountCurrency, String user) {
        this.number = accountNumber;
        this.currency = accountCurrency;
        this.user = user;
    }
    
    public Account(String accountNumber,int id, String accountCurrency, String user) {
        this.id = id;
    	this.number = accountNumber;
        this.currency = accountCurrency;
        this.user = user;
    }
    public Account (){
    	//dummy constructor
    }
}
