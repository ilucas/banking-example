package com.example.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class Credentials {

    @JsonProperty
    int id;

    @NotNull
    @JsonProperty
    String email;

    @NotNull
    @JsonProperty
    String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Credentials() {}

    public Credentials(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
