package com.example.api.security;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebToken implements HttpSessionBindingListener {

    
    @NotNull
    @JsonProperty
	String username;
    
    @JsonProperty
    DateTime time;

    public String getUsername() {
		return username;
	}

	public void setUsername(String user) {
		this.username = user;
	}
    
    public WebToken(String user, DateTime time) {
        this.username = user;
        this.time = time;
    }
    public WebToken (){
    	//dummy constructor
    }
    
    @Override
    public String toString() {

    	return username + " " + time;
    }

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		
		//fireSessionTerminated(this);
		
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		
		//this method is called when the http session expires, this means that the user has to log in again
		
		//here I should notify the session service that this user's token has expired
		System.out.println("gigi kent");
		
		//fireSessionTerminated(this);
		
	}
}
