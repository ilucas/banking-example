package com.example.api.security.jwt;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

import com.example.api.security.jwt.annotation.AuthRequired;
import com.example.api.security.jwt.beans.Privilege;
import com.example.api.security.jwt.beans.User;
import com.example.api.security.jwt.configuration.JwtBaseConfiguration;

import jwt4j.JWTHandler;

public class AuthDynamicFeature implements DynamicFeature
{
    private final JwtBaseConfiguration configuration;

    private final JWTHandler<User> jwtHandler;

    public AuthDynamicFeature(final JwtBaseConfiguration configuration,
                              final JWTHandler<User> jwtHandler)
    {
        this.configuration = configuration;
        this.jwtHandler = jwtHandler;
    }

    public void configure(ResourceInfo resourceInfo, FeatureContext featureContext)
    {
        final Method resourceMethod = resourceInfo.getResourceMethod();
        if (resourceMethod != null) {
            Stream.of(resourceMethod.getParameterAnnotations())
                    .flatMap(Arrays::stream)
                    .filter(annotation -> annotation.annotationType().equals(AuthRequired.class))
                    .map(AuthRequired.class::cast)
                    .findFirst()
                    .ifPresent(authRequired -> featureContext.register(getAuthFilter(authRequired.value())));
        }
    }

    private ContainerRequestFilter getAuthFilter(final Privilege[] requiredPrivileges)
    {
        /*return containerRequestContext -> {
            final String authHeader = containerRequestContext.getHeaderString(configuration.getAuthHeader());
            containerRequestContext.getPropertyNames();
            if (authHeader == null) {
                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
            }
            final User user;
            try {
                user = jwtHandler.decode(authHeader);
            } catch (Exception e) {
                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
            }
            if (!user.getPrivileges().containsAll(Arrays.asList(requiredPrivileges))) {
                throw new WebApplicationException(Response.Status.FORBIDDEN);
            }
            containerRequestContext.setProperty("user", user);
        };*/
    	
    	return new MyRequestFilter(requiredPrivileges,jwtHandler,configuration);
    	//return new MyRequestFilter();
    }
    
    
}