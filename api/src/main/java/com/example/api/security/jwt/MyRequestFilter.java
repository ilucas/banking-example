package com.example.api.security.jwt;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.example.api.security.jwt.beans.Privilege;
import com.example.api.security.jwt.beans.User;
import com.example.api.security.jwt.configuration.JwtBaseConfiguration;

import jwt4j.JWTHandler;

public class MyRequestFilter implements ContainerRequestFilter {
	
	//@Context private HttpServletRequest request;
	@Context
    private HttpServletRequest servletRequest;
	
	private Privilege[] requiredPrivileges;
	private JWTHandler<User> jwtHandler;
	private JwtBaseConfiguration configuration;
	
	
	public MyRequestFilter(){
		//nothing, just to register it.
	}
	
	public MyRequestFilter(Privilege[] requiredPrivileges, JWTHandler<User> jwtHandler, JwtBaseConfiguration configuration){
		
		this.requiredPrivileges = requiredPrivileges;
		this.jwtHandler = jwtHandler;
		this.configuration = configuration;
	}
	
	@Override
	public void filter(ContainerRequestContext containerRequestContext) throws IOException {
		
		String authHeader ;//= containerRequestContext.getHeaderString(configuration.getAuthHeader());
		
		//reassign from session
		authHeader = (String) servletRequest.getSession().getAttribute(configuration.getAuthHeader());
		//containerRequestContext.setSecurityContext();
        if (authHeader == null) {
            //throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        	//throw new WebApplicationException( Response.status(Status.OK).entity(new LoginView(""))
				//	.build());
        	containerRequestContext.setProperty("user", new User("user@test.com", new HashSet<>(Arrays.asList(Privilege.USER))));
        	return;
        }
        final User user;
        try {
            user = jwtHandler.decode(authHeader);
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
        if (!user.getPrivileges().containsAll(Arrays.asList(requiredPrivileges))) {
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }
        containerRequestContext.setProperty("user", user);
	}
	
}