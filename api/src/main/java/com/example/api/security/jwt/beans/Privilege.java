package com.example.api.security.jwt.beans;

public enum Privilege
{
    USER, MODERATOR, ADMINISTRATOR
}