package com.example.frontend;

import java.util.Map;
import java.util.stream.Stream;

import javax.ws.rs.client.Client;

import org.eclipse.jetty.server.session.SessionHandler;

import com.example.api.security.jwt.AuthDynamicFeature;
import com.example.api.security.jwt.AuthValueFactoryProvider;
import com.example.api.security.jwt.MyRequestFilter;
import com.example.api.security.jwt.beans.User;
import com.example.api.security.jwt.configuration.JwtBaseConfiguration;
import com.example.frontend.configuration.FrontendConfiguration;
import com.example.frontend.resources.FrontendResource;
import com.example.frontend.services.AccountService;

import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jersey.sessions.HttpSessionFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import jwt4j.JWTHandler;
import jwt4j.JWTHandlerBuilder;

public class FrontendApplication extends Application<FrontendConfiguration> {
    public static void main(String[] args) throws Exception {
        new FrontendApplication().run(args);
    }

    @Override
    public String getName() {
        return "frontend-application";
    }

    @Override
    public void initialize(Bootstrap<FrontendConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<FrontendConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(FrontendConfiguration configuration) {
                return configuration.getViewRendererConfiguration();
            }
        });
    }

    @Override
    public void run(FrontendConfiguration configuration, Environment environment) {
      
    	final Client accountClient = new JerseyClientBuilder(environment)
            .using(configuration.getHttpAccountClient())
            .build("accountClient");
    	
    	environment.jersey().register(HttpSessionFactory.class);
        environment.servlets().setSessionHandler(new SessionHandler());

        final AccountService accountService = new AccountService(accountClient, configuration.getAccountApiUri());
                
        final JWTHandler<User> jwtHandler = getJwtHandler(configuration);
        final FrontendResource frontendResource = new FrontendResource(configuration,accountService,jwtHandler);
        
        //environment.jersey().register(new MyRequestFilter());
        
        
        
        Stream.of(frontendResource,
        		new FrontendExceptionMapper(),
        		new AuthDynamicFeature(configuration, jwtHandler),
                new AuthValueFactoryProvider.Binder()).forEach(environment.jersey()::register);
        
        
    }
    
    private JWTHandler<User> getJwtHandler(JwtBaseConfiguration jwtApplicationConfiguration)
    {
        return new JWTHandlerBuilder<User>()
                .withSecret(jwtApplicationConfiguration.getAuthSalt().getBytes())
                .withDataClass(User.class)
                .build();
    }
}
