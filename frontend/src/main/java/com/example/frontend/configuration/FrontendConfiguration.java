package com.example.frontend.configuration;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.example.api.security.jwt.configuration.JwtBaseConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.client.JerseyClientConfiguration;

public class FrontendConfiguration extends JwtBaseConfiguration {

    @NotNull
    @JsonProperty("viewRendererConfiguration")
    private Map<String, Map<String, String>> viewRendererConfiguration;

    public Map<String, Map<String, String>> getViewRendererConfiguration() {
        return viewRendererConfiguration;
    }

    @Valid
    @NotNull
    @JsonProperty("httpAccountClient")
    private JerseyClientConfiguration httpAccountClient = new JerseyClientConfiguration();

    public JerseyClientConfiguration getHttpAccountClient() {
        return httpAccountClient;
    }

    @NotNull
    @JsonProperty("accountApiUri")
    private String accountApiUri;

    public String getAccountApiUri() {
        return accountApiUri;
    }
    
    @NotNull
    @JsonProperty("authenticationServiceUri")
    private String authenticationServiceUri;

    public String getAuthenticationServiceUri() {
        return authenticationServiceUri;
    }
}
