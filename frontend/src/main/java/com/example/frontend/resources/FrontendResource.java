package com.example.frontend.resources;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.api.model.Account;
import com.example.api.security.WebToken;
import com.example.api.security.jwt.annotation.AuthRequired;
import com.example.api.security.jwt.beans.Privilege;
import com.example.api.security.jwt.beans.User;
import com.example.api.security.jwt.beans.UserCredentials;
import com.example.frontend.configuration.FrontendConfiguration;
import com.example.frontend.services.AccountService;
import com.example.frontend.views.AccountView;
import com.example.frontend.views.AccountsEditView;
import com.example.frontend.views.AccountsView;
import com.example.frontend.views.ErrorView;
import com.example.frontend.views.IndexView;
import com.example.frontend.views.LoginView;

import io.dropwizard.jersey.sessions.Session;
import io.dropwizard.views.View;
import jwt4j.JWTHandler;

@Path("/")
@Produces({ MediaType.TEXT_HTML })
public class FrontendResource {


	private static final Logger LOGGER = LoggerFactory.getLogger(FrontendResource.class);
	
	private static final int _MAX_INACTIVE_INTERVAL = 30;
	
    private static final Map<UserCredentials, User> USERS = Stream.of(
            new SimpleEntry<>(new UserCredentials("user", "123"),
                    new User("just-user", new HashSet<>(Arrays.asList(Privilege.USER)))),
            new SimpleEntry<>(new UserCredentials("moderator", "1234"),
                    new User("moderator", new HashSet<>(Arrays.asList(Privilege.USER, Privilege.MODERATOR)))),
            new SimpleEntry<>(new UserCredentials("administrator", "12345"),
                    new User("administrator", new HashSet<>(Arrays.asList(Privilege.values())))),
            new SimpleEntry<>(new UserCredentials("user@test.com", "user"),
                    new User("user@test.com", new HashSet<>(Arrays.asList(Privilege.USER)))),
            new SimpleEntry<>(new UserCredentials("admin@test.com", "admin"),
                    new User("admin@test.com", new HashSet<>(Arrays.asList(Privilege.values()))))
    ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));

    private final JWTHandler<User> jwtHandler;
	
	final AccountService accountService;
	
	//http header name or session variable name
	private String AUTH_HEADER;

	public FrontendResource(FrontendConfiguration configuration, AccountService accountService,final JWTHandler<User> jwtHandler) {
		this.accountService = accountService;
		this.AUTH_HEADER = configuration.getAuthHeader();
		this.jwtHandler = jwtHandler;
	}

	@GET
	public View index(@Session HttpSession session,@AuthRequired User user) {
		//return login(session);
		
		
		List<Account> accounts = accountService.getAll(user.getUsername());
		return new AccountsView(user.getUsername(), accounts);
	}

	@GET
	@Path("/login")
	public View login(@Session HttpSession session) {
		
		User user;
		try{
		
			user = jwtHandler.decode((String) session.getAttribute(AUTH_HEADER));
		}
		catch( jwt4j.exceptions.InvalidTokenException e){
			return new LoginView("");
		}
		
		//if (user == null)
			//return new LoginView("");
			/*throw new WebApplicationException( Response.status(Status.OK).entity(new LoginView(""))
					.build());*/

		List<Account> accounts = accountService.getAll(user.getUsername());
		return new AccountsView(user.getUsername(), accounts);
	}
	
	@POST
	@Path("/login")
	public View login(@FormParam("emailaddress") String emailaddress, @FormParam("password") String password,
			@Session HttpSession session) {

		//todo: sanitize
		
		
		//verify
    	UserCredentials userCredentials = new UserCredentials(emailaddress, password);
		
    	if (USERS.containsKey(userCredentials)) {
              	
    			//ADD TOKEN IN SESSION
    			String token = jwtHandler.encode(USERS.get(userCredentials));
    			
    			session.setMaxInactiveInterval(_MAX_INACTIVE_INTERVAL);
    			session.setAttribute(AUTH_HEADER, token);
    			return defaultView(emailaddress);
    			
        } 

    	//throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        //return new LoginView("", "User/pass not correct");
    	throw new WebApplicationException( Response.status(Status.UNAUTHORIZED).entity(new LoginView(""))
				.build());

	}
	
	/*@POST
	@Path("/login")
	public View login(@FormParam("emailaddress") String emailaddress, @FormParam("password") String password,
			@Session HttpSession session) {

		LOGGER.info("Received authentication attempt for email: " + emailaddress);
		
		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token != null){
			List<Account> accounts = accountService.getAll(token.getUsername());
			return new AccountsView(emailaddress, accounts);
		}
			
		
		// set session stuff:

		if ((emailaddress.equals("admin@test.com") && password.equals("admin")) ||
				emailaddress.equals("user@test.com") && password.equals("user")) {

			token = new WebToken(emailaddress, DateTime.now());
			session.setMaxInactiveInterval(_MAX_INACTIVE_INTERVAL);
			session.setAttribute(AUTH_HEADER, token);
			
			LOGGER.info("!!!!!!!!!!! adding session variable <token> " + session.getAttribute(AUTH_HEADER));

			return defaultView(emailaddress);

		}

		return new LoginView("", "User/pass not correct");
	}*/

	private View defaultView(String emailaddress) {
		List<Account> accounts = accountService.getAll(emailaddress);
		return new AccountsView(emailaddress, accounts);
	}

	@POST
	@Path("/logout")
	public View logout(@Session HttpSession session, @Context Servlet servlet) {


		User user;
		try{
		
			user = jwtHandler.decode((String) session.getAttribute(AUTH_HEADER));
		}
		catch( jwt4j.exceptions.InvalidTokenException e){
			return new LoginView("");
		}

		LOGGER.info("Logging out: " + user.getUsername());
		session.invalidate();
		return new IndexView("");
	}

	@GET
	@Path("/accounts")
	public View accounts(@Session HttpSession session) {

		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token == null)
			return new LoginView("", "You are NOT logged in");

		LOGGER.info("Getting accounts list for callerId: " + token.getUsername());

		List<Account> accounts = accountService.getAll(token.getUsername());
		return new AccountsView(token.getUsername(), accounts);
	}

	@GET
	@Path("/accounts/add")
	public View accountsAdd(@Session HttpSession session) {

		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token == null)
			return new LoginView("", "You are NOT logged in");

		return new AccountsEditView(token.getUsername(), null, true, null);
	}

	@POST
	@Path("/accounts/add")
	public View accountsAddSubmit(@Session HttpSession session, @FormParam("number") String number,
			@FormParam("currency") String currency) {

		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token == null)
			return new LoginView("", "You are NOT logged in");

		LOGGER.info("Adding account for callerId: " + token);

		Account account = new Account(number, currency, token.getUsername());
		// in real application validation would occur here, api errors caught
		// and return view with errors
		try {
			accountService.post(token.getUsername(), account);
		} catch (NotAuthorizedException notAuthorizedException) {
			return new AccountsEditView(token.getUsername(), account, true, new String[] { "notAuthorised" });
		}

		List<Account> accounts = accountService.getAll(token.getUsername());
		return new AccountsView(token.getUsername(), accounts);

		// throw new
		// WebApplicationException(Response.seeOther(UriBuilder.fromUri("/accounts").build()).build());
	}

	@GET
	@Path("/accounts/{accountId}")
	public View getAccount(@Session HttpSession session, @PathParam("accountId") int accountId) {

		LOGGER.info("!!!!!!!!!!! checking session value <token>" + session.getAttribute(AUTH_HEADER));
		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token == null)
			return new LoginView("", "You are NOT logged in");

		Account account = accountService.get(token.getUsername(), accountId);
		return new AccountView(token.getUsername(), account);
	}

	@GET
	@Path("/accounts/{accountId}/edit")
	public View accountEdit(@Session HttpSession session, @PathParam("accountId") int accountId) {

		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token == null)
			return new LoginView("", "You are NOT logged in");

		Account account = accountService.get(token.getUsername(), accountId);

		return new AccountsEditView(token.getUsername(), account, false, null);
	}

	@POST
	@Path("/accounts/{accountId}/edit")
	public View accountEditSubmit(@Session HttpSession session, @PathParam("accountId") int accountId,
			@FormParam("number") String number, @FormParam("currency") String currency) {

		WebToken token = (WebToken) session.getAttribute(AUTH_HEADER);
		if (token == null)
			return new LoginView("", "You are NOT logged in");

		LOGGER.info("Updating account for callerId: " + token.getUsername());

		Account account = new Account(number, currency, token.getUsername());
		account.setId(accountId);
		// in real application validation would occur here, api errors caught
		// and return view with errors
		try {
			accountService.put(token.getUsername(), account);
		} catch (NotAuthorizedException notAuthorizedException) {
			return new AccountsEditView(token.getUsername(), account, true, new String[] { "notAuthorised" });
		}

		/*
		 * throw new WebApplicationException(
		 * Response.seeOther(UriBuilder.fromUri("/accounts/" +
		 * accountId).build()).build());
		 */

		List<Account> accounts = accountService.getAll(token.getUsername());
		return new AccountsView(token.getUsername(), accounts);
	}

	@GET
	@Path("/401")
	public View get401(@Session HttpSession session) {

		return new ErrorView("/templates/partials/401.ftl", "");
	}
	
	@GET
	@Path("/405")
	public View get405(@Session HttpSession session) {

		return new ErrorView("/templates/partials/401.ftl", "");
	}
}