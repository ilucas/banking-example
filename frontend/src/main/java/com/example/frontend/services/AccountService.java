package com.example.frontend.services;

import com.example.api.model.Account;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class AccountService {

    final Client client;
    final String baseUri;
    final WebTarget accountTarget;

    public AccountService(Client client, String baseUri) {
        this.client = client;
        this.baseUri = baseUri;

        this.accountTarget = client.target(baseUri);
    }

    public List<Account> getAll(String callerId) {
        return accountTarget
            .request(MediaType.APPLICATION_JSON)
            .header("callerId", callerId)
            .get(new GenericType<List<Account>>() {});
    }

    public Account get(String callerId, int accountId) {
        
    	return accountTarget
            .path("/" + accountId)
            .request(MediaType.APPLICATION_JSON)
            .header("callerId", callerId)
            .get(Account.class);
    }

    public Account post(String callerId, Account account) {
        return accountTarget
            .request(MediaType.APPLICATION_JSON)
            .header("callerId", callerId)
            .post(Entity.entity(account, MediaType.APPLICATION_JSON), Account.class);
    }

    public Account put(String callerId, Account account) {
        return accountTarget
            .path("/" + account.getId())
            .request(MediaType.APPLICATION_JSON)
            .header("callerId", callerId)
            .put(Entity.entity(account, MediaType.APPLICATION_JSON), Account.class);
    }
}
