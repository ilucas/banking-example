package com.example.frontend.views;

import com.example.api.model.Account;

public class AccountView extends BaseView {

    Account account;

    public Account getAccount() {
        return account;
    }

    public AccountView(String callerId, Account account) {
        super("/templates/partials/account.ftl", callerId);
        this.account = account;
    }
}
