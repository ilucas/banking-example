package com.example.frontend.views;

import com.example.api.model.Account;

public class AccountsEditView extends BaseView {

    Account account;
    boolean isNew;
    String[] errors = null;

    public Account getAccount() {
        return account;
    }

    public boolean getIsNew() {
        return isNew;
    }

    public String[] getErrors() {
        return errors;
    }

    public AccountsEditView(String callerId, Account account, boolean isNew, String[] errors) {
        super("/templates/partials/accountEdit.ftl", callerId);
        this.account = account;
        this.isNew = isNew;
        this.errors = errors;
    }
}
