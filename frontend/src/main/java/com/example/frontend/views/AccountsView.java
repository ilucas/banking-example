package com.example.frontend.views;

import com.example.api.model.Account;

import java.util.List;

public class AccountsView extends BaseView {

    List<Account> accounts;

    public List<Account> getAccounts() {
        return accounts;
    }

    public AccountsView(String callerId, List<Account> accounts) {
        super("/templates/partials/accounts.ftl", callerId);
        this.accounts = accounts;
    }
}
