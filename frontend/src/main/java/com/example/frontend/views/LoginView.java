package com.example.frontend.views;

public class LoginView extends BaseView {
	
	String message;
	
    public String getMessage() {
		return message;
	}


	public LoginView(String callerId,String message) {
        this(callerId);
        this.message = message;
    }
	
	public LoginView(String callerId) {
		super("/templates/partials/login.ftl", callerId);
    }
}
