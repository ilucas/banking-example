package com.example.frontend.views;

public class SuccessView extends BaseView {
	
	
    String email;

	public SuccessView(String callerId,String email,String password) {
        super("/templates/partials/success.ftl",callerId);
        this.email = "Successfully logged in with admin rights: " + email;
    }

	public String getEmail() {
		return email;
	}

}
