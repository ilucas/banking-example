<#include "../main.ftl">
<#macro page_body>

<h1><a href="/accounts">Accounts</a> - ${account.id}</h1>

<form class="form-horizontal">
  <div class="form-group">
    <label class="col-sm-2 control-label">Number</label>
    <div class="col-sm-10">
      <p class="form-control-static">${account.number}</p>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Currency</label>
    <div class="col-sm-10">
      <p class="form-control-static">${account.currency}</p>
    </div>
  </div>
</form>

<a href="/accounts/${account.id}/edit">Edit Account</a>

</#macro>
