<#include "../main.ftl">
<#macro page_body>

<h1><a href="/accounts">Accounts</a> - <#if isNew>new<#else>${account.id}</#if></h1>



<form class="form-horizontal" method="POST">
  <div class="form-group">
    <label for="number" class="col-sm-2 control-label">Number</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="number" name="number" placeholder="number" <#if account??>value="${account.number}"</#if>>
    </div>
  </div>
  <div class="form-group">
    <label for="currency" class="col-sm-2 control-label">Currency</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="currency" name="currency" placeholder="Currency" <#if account??>value="${account.currency}"</#if>>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
      <#if errors?? && errors?seq_contains("notAuthorised")>
      <span class="has-error">
        <label class="control-label">You do not have permission to do that</label>
      </span>
      </#if>
    </div>
  </div>
</form>

</#macro>
