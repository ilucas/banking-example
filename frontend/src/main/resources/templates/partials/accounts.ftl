<#include "../main.ftl">
<#macro page_body>

<h1>Accounts</h1>

<#if accounts?has_content>
<ul>
<#list accounts as account>
  <li><a href="/accounts/${account.id}">${account.number} ${account.currency}</a></li>
</#list>
</ul>
</#if>

<a href="/accounts/add">Add Account</a>

</#macro>
